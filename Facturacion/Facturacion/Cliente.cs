﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Facturacion
{
    public partial class Cliente : Form
    {

        String sSql;

         SqlConnection oCon = new SqlConnection(@"server=.\YARITZA; Initial Catalog = sistemafacturacion ; Integrated Security=True;");
        

         DataTable oDt = new DataTable();
        public Cliente()
        {
            InitializeComponent();

            
        }
        private void cbxEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void Cliente_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'sistemafacturacionDataSet.Cliente' Puede moverla o quitarla según sea necesario.
            this.clienteTableAdapter.Fill(this.sistemafacturacionDataSet.Cliente);
            cbxEstado.SelectedIndex = 0;
        }

        private void btnGuadar_Click(object sender, EventArgs e)
        {
             try
            {
                SqlDataAdapter oDa = new SqlDataAdapter(sSql, oCon);
                oCon.Open();


             sSql= "insert into Cliente values ('" +txtID.Text + "','" +  txtDescripcion.Text + "', '" + txtNoIdentidad.Text + "','" + txtCuentaContable.Text + "','" + cbxEstado.Text.Substring(0, 1) + "');";
         SqlCommand oCmd = new SqlCommand(sSql, oCon);
                 oCmd.ExecuteNonQuery();
                 MessageBox.Show("Registro guardado exitosamente");

                 sSql = "Select * from Cliente order by ID";
                 
                 oDa.Fill(oDt);
                 dgvCliente.DataSource = oDt;
                 dgvCliente.Refresh();
             
             }
            catch(Exception ex){
                MessageBox.Show("Error al guardar " + ex.Message);
            }

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            borrar();
            SqlDataAdapter oDa = new SqlDataAdapter(sSql, oCon);
            SqlCommand bor = new SqlCommand();
           
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM CLIENTE", oCon);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dgvCliente.DataSource = dt;
            oCon.Close();

        }
        private void borrar()

        {
            int i = 0;
            oCon.Open();
            SqlCommand storedProcedureCommand = oCon.CreateCommand();
            storedProcedureCommand.CommandType = CommandType.Text;
            storedProcedureCommand.CommandText = "DELETE FROM CLIENTE WHERE ID = @ID";
            SqlParameter inparam2 = new SqlParameter("@ID", SqlDbType.Int);
            inparam2.Direction = ParameterDirection.Input;
            inparam2.Value = Convert.ToInt32(dgvCliente.Rows[i].Cells[0].Value);
            storedProcedureCommand.Parameters.Add(inparam2);
            storedProcedureCommand.ExecuteNonQuery();
            oCon.Close();
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

       

    }
}
