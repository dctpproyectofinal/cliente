﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;//necesario para poder acceder a SQL

namespace Facturacion
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();

            txtContrasena.PasswordChar = '*';
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try{
                //creando la conexion

            SqlConnection miConeccion = new SqlConnection(@"server=.\YARITZA; Initial Catalog = loginbd; Integrated Security=True;");
            miConeccion.Open();
            SqlCommand comando = new SqlCommand("select usuario, contraseña from Tblusuario where usuario = '" + txtUsuario.Text + "'And contraseña = '" + txtContrasena.Text + "' ", miConeccion);
            //ejecuta una instruccion de sql devolviendo el numero de las filas afectadas
            comando.ExecuteNonQuery();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(comando);

            //Llenando el dataAdapter
            da.Fill(ds, "Tblusuario");
            //utilizado para representar una fila de la tabla q necesitas en este caso usuario
            DataRow DR;
            DR = ds.Tables["Tblusuario"].Rows[0];
            
            //evaluando que la contraseña y usuario sean correctos
            if ((txtUsuario.Text == DR["usuario"].ToString()) || (txtContrasena.Text == DR["contraseña"].ToString()))
            {
                //instanciando el formulario principal
                 FrmPrincipal frm = new FrmPrincipal();
                frm.Show();//abriendo el formulario principal
                this.Hide();//esto sirve para ocultar el formulario de login


            }
        }
          catch{
//en caso que la contraseña sea erronea mostrara un mensaje
//dentro de los parentesis va: "Mensaje a mostrar","Titulo de la ventana",botones a mostrar en ste caso OK, icono a mostrar en este caso uno de error
MessageBox.Show("Error! Su contraseña y/o usuario son invalidos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
}

}
           
            

        

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit(); 
        }

        private void txtUsuario_TextChanged(object sender, EventArgs e)
        {

            //aca se activa el boton INICIAR

            btnLogin.Enabled = true;
        }
    }
}
